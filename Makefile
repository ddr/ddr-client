SHELL = /bin/bash

swagger_codegen_version ?= 3.0.54
swagger_codegen_image ?= swaggerapi/swagger-codegen-cli-v3:$(swagger_codegen_version)
swagger_doc ?= https://ddr-admin.lib.duke.edu/api/openapi.json

.PHONY: build
build: clean
	docker pull $(swagger_codegen_image)
	docker run --rm -w /src -v "$(shell pwd):/src" $(swagger_codegen_image) \
		generate -i $(swagger_doc) -l ruby -c config.json
	rm -f git_push.sh

.PHONY: update
update: build test audit
	git add .
	git commit -am "Updated $(shell date -I)"
	git push $(shell git remote) $(shell git branch --show-current)

.PHONY: clean
clean:
	rm -rf Gemfile.lock lib/ spec/ docs/

.PHONY: test
test:
	ruby_version=3.1 ./test.sh
	ruby_version=3.2 ./test.sh
	ruby_version=3.3 ./test.sh

.PHONY: audit
audit:
	docker run --rm -v "$(shell pwd):/src" -w /src -u 0 ruby:3.2 /bin/bash -c \
		'apt-get -y update && apt-get -y install jq && ./audit.sh'
