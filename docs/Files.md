# DdrClient::Files

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**caption** | [**CaptionFile**](CaptionFile.md) |  | [optional] 
**content** | [**OriginalFile**](OriginalFile.md) |  | [optional] 
**derived_image** | [**DerivedImage**](DerivedImage.md) |  | [optional] 
**extracted_text** | [**ExtractedText**](ExtractedText.md) |  | [optional] 
**fits_file** | [**FITSFile**](FITSFile.md) |  | [optional] 
**intermediate_file** | [**IntermediateFile**](IntermediateFile.md) |  | [optional] 
**multires_image** | [**MultiresolutionImage**](MultiresolutionImage.md) |  | [optional] 
**streamable_media** | [**StreamableMedia**](StreamableMedia.md) |  | [optional] 
**struct_metadata** | [**StructuralMetadata**](StructuralMetadata.md) |  | [optional] 
**thumbnail** | [**ThumbnailImage**](ThumbnailImage.md) |  | [optional] 

