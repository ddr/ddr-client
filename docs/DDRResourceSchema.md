# DdrClient::DDRResourceSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [****](.md) | Unique resource identifier assigned by the repository | [optional] 
**model** | [****](.md) | The model type of the resource | [optional] 
**created_at** | [****](.md) | Creation time of the database record (may differ from ingestion date) | [optional] 
**updated_at** | [****](.md) | Modification time of the database record | [optional] 
**optimistic_lock_token** | [****](.md) | Token required for update with optimistic locking | [optional] 
**metadata** | [**MetadataFields**](MetadataFields.md) |  | [optional] 
**roles** | [****](.md) | Role-based access control assertions on the resource | [optional] 
**files** | [**Files**](Files.md) |  | [optional] 
**related** | [**RelatedResourceIDs**](RelatedResourceIDs.md) |  | [optional] 

