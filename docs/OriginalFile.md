# DdrClient::OriginalFile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**original_filename** | [****](.md) | Original name of file at time of ingest | 
**media_type** | [****](.md) | Standard internet media type descriptor for file content | [optional] 
**file_path** | [****](.md) | Path to the file on a storage device | [optional] 
**sha1** | [****](.md) | SHA1 digest of file contents | [optional] 
**created** | [****](.md) | Creation date/time of the file in the repository (not creation time of original file) | [optional] 
**updated** | [****](.md) | Modification date/time of the file in the repository (not modification time of original file) | [optional] 

