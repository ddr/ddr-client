# DdrClient::AdminSetsApi

All URIs are relative to *https://ddr-admin.lib.duke.edu/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_admin_set**](AdminSetsApi.md#get_admin_set) | **GET** /admin_sets/{code} | 
[**get_admin_sets**](AdminSetsApi.md#get_admin_sets) | **GET** /admin_sets | 

# **get_admin_set**
> AdminSet get_admin_set



Retrieve admin set

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::AdminSetsApi.new

begin
  result = api_instance.get_admin_set
  p result
rescue DdrClient::ApiError => e
  puts "Exception when calling AdminSetsApi->get_admin_set: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AdminSet**](AdminSet.md)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_admin_sets**
> get_admin_sets



List admin sets

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::AdminSetsApi.new

begin
  api_instance.get_admin_sets
rescue DdrClient::ApiError => e
  puts "Exception when calling AdminSetsApi->get_admin_sets: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



