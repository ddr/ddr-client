# DdrClient::IndexApi

All URIs are relative to *https://ddr-admin.lib.duke.edu/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_index_doc**](IndexApi.md#delete_index_doc) | **DELETE** /index/{id} | 
[**get_index_doc**](IndexApi.md#get_index_doc) | **GET** /index/{id} | 
[**get_index_schema**](IndexApi.md#get_index_schema) | **GET** /index/schema | 

# **delete_index_doc**
> delete_index_doc



Delete index document

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::IndexApi.new

begin
  api_instance.delete_index_doc
rescue DdrClient::ApiError => e
  puts "Exception when calling IndexApi->delete_index_doc: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_index_doc**
> get_index_doc



Retrieve index document

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::IndexApi.new

begin
  api_instance.get_index_doc
rescue DdrClient::ApiError => e
  puts "Exception when calling IndexApi->get_index_doc: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_index_schema**
> get_index_schema



Retrieve index schema

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::IndexApi.new

begin
  api_instance.get_index_schema
rescue DdrClient::ApiError => e
  puts "Exception when calling IndexApi->get_index_schema: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



