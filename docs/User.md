# DdrClient::User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [****](.md) |  | [optional] 
**username** | [****](.md) |  | 
**netid** | [****](.md) |  | [optional] 
**duid** | [****](.md) |  | [optional] 
**email** | [****](.md) |  | 
**first_name** | [****](.md) |  | [optional] 
**middle_name** | [****](.md) |  | [optional] 
**nickname** | [****](.md) |  | [optional] 
**last_name** | [****](.md) |  | [optional] 
**display_name** | [****](.md) |  | [optional] 
**created_at** | [****](.md) |  | [optional] 
**updated_at** | [****](.md) |  | [optional] 

