# DdrClient::QueuesApi

All URIs are relative to *https://ddr-admin.lib.duke.edu/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_queues_locks**](QueuesApi.md#delete_queues_locks) | **DELETE** /queues/locks | 
[**get_queues_stats**](QueuesApi.md#get_queues_stats) | **GET** /queues/stats | 

# **delete_queues_locks**
> delete_queues_locks



Unlock locked jobs

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::QueuesApi.new

begin
  api_instance.delete_queues_locks
rescue DdrClient::ApiError => e
  puts "Exception when calling QueuesApi->delete_queues_locks: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_queues_stats**
> get_queues_stats



Retrieve queue stats

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::QueuesApi.new

begin
  api_instance.get_queues_stats
rescue DdrClient::ApiError => e
  puts "Exception when calling QueuesApi->get_queues_stats: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined



