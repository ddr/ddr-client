# DdrClient::ResourcesApi

All URIs are relative to *https://ddr-admin.lib.duke.edu/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_resource**](ResourcesApi.md#get_resource) | **GET** /resources/{id} | 
[**get_resource_attachments**](ResourcesApi.md#get_resource_attachments) | **GET** /resources/{id}/attachments | 
[**get_resource_batches**](ResourcesApi.md#get_resource_batches) | **GET** /resources/{id}/batches | 
[**get_resource_children**](ResourcesApi.md#get_resource_children) | **GET** /resources/{id}/children | 
[**get_resource_download**](ResourcesApi.md#get_resource_download) | **GET** /resources/{id}/download | 
[**get_resource_file**](ResourcesApi.md#get_resource_file) | **GET** /resources/{id}/files/{file_field} | 
[**get_resource_files**](ResourcesApi.md#get_resource_files) | **GET** /resources/{id}/files | 
[**get_resource_fixity**](ResourcesApi.md#get_resource_fixity) | **GET** /resources/{id}/fixity | 
[**get_resource_fixity_check**](ResourcesApi.md#get_resource_fixity_check) | **GET** /resources/{id}/fixity_check | 
[**get_resource_members**](ResourcesApi.md#get_resource_members) | **GET** /resources/{id}/members | 
[**get_resource_permanent_id**](ResourcesApi.md#get_resource_permanent_id) | **GET** /resources/{id}/permanent_id | 
[**get_resource_permissions**](ResourcesApi.md#get_resource_permissions) | **GET** /resources/{id}/permissions | 
[**get_resource_targets**](ResourcesApi.md#get_resource_targets) | **GET** /resources/{id}/targets | 
[**get_resource_technical_metadata**](ResourcesApi.md#get_resource_technical_metadata) | **GET** /resources/{id}/technical_metadata | 
[**get_resources**](ResourcesApi.md#get_resources) | **GET** /resources | 
[**get_resources_by_role**](ResourcesApi.md#get_resources_by_role) | **GET** /resources/by_role | 
[**patch_resource**](ResourcesApi.md#patch_resource) | **PATCH** /resources/{id} | 
[**post_resource_technical_metadata**](ResourcesApi.md#post_resource_technical_metadata) | **POST** /resources/{id}/technical_metadata | 
[**post_resources**](ResourcesApi.md#post_resources) | **POST** /resources | 
[**put_resource_file**](ResourcesApi.md#put_resource_file) | **PUT** /resources/{id}/files/{file_field} | 

# **get_resource**
> DDRResourceSchema get_resource(opts)



Retrieve resource

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::ResourcesApi.new
opts = { 
  fields: DdrClient::null.new, #  | Comma-separated list of metadata fields to include in the response. All fields are included if the parameter is omitted, unless the 'exclude_fields' parameter is used.
  exclude_fields: DdrClient::null.new #  | Comma-separated list of metadata fields to exclude from the response. Cannot be used in combination with the 'fields' parameter.
}

begin
  result = api_instance.get_resource(opts)
  p result
rescue DdrClient::ApiError => e
  puts "Exception when calling ResourcesApi->get_resource: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | [****](.md)| Comma-separated list of metadata fields to include in the response. All fields are included if the parameter is omitted, unless the &#x27;exclude_fields&#x27; parameter is used. | [optional] 
 **exclude_fields** | [****](.md)| Comma-separated list of metadata fields to exclude from the response. Cannot be used in combination with the &#x27;fields&#x27; parameter. | [optional] 

### Return type

[**DDRResourceSchema**](DDRResourceSchema.md)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_resource_attachments**
> get_resource_attachments(opts)



Retrieve resource attachments

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::ResourcesApi.new
opts = { 
  fields: DdrClient::null.new, #  | Comma-separated list of metadata fields to include in the response. All fields are included if the parameter is omitted, unless the 'exclude_fields' parameter is used.
  exclude_fields: DdrClient::null.new, #  | Comma-separated list of metadata fields to exclude from the response. Cannot be used in combination with the 'fields' parameter.
  csv_fields: DdrClient::null.new, #  | Comma-separated list of additional fields to include in the CSV response
  file_fields: DdrClient::null.new, #  | Comma-separated list of file fields to include in the CSV response
  remove_empty_columns: DdrClient::null.new, #  | Remove empty columns from the CSV response
  page: DdrClient::null.new, #  | Page number of results to return (default: 1) in the JSON response
  per_page: DdrClient::null.new #  | Number of records to return per page (default: 20) in the JSON response
}

begin
  api_instance.get_resource_attachments(opts)
rescue DdrClient::ApiError => e
  puts "Exception when calling ResourcesApi->get_resource_attachments: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | [****](.md)| Comma-separated list of metadata fields to include in the response. All fields are included if the parameter is omitted, unless the &#x27;exclude_fields&#x27; parameter is used. | [optional] 
 **exclude_fields** | [****](.md)| Comma-separated list of metadata fields to exclude from the response. Cannot be used in combination with the &#x27;fields&#x27; parameter. | [optional] 
 **csv_fields** | [****](.md)| Comma-separated list of additional fields to include in the CSV response | [optional] 
 **file_fields** | [****](.md)| Comma-separated list of file fields to include in the CSV response | [optional] 
 **remove_empty_columns** | [****](.md)| Remove empty columns from the CSV response | [optional] [default to false]
 **page** | [****](.md)| Page number of results to return (default: 1) in the JSON response | [optional] 
 **per_page** | [****](.md)| Number of records to return per page (default: 20) in the JSON response | [optional] 

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined



# **get_resource_batches**
> get_resource_batches(opts)



Retrieve resource batches

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::ResourcesApi.new
opts = { 
  batch_objects: DdrClient::null.new, #  | Include batch objects
  page: DdrClient::null.new, #  | Page number of results to return (default: 1) in the JSON response
  per_page: DdrClient::null.new #  | Number of records to return per page (default: 20) in the JSON response
}

begin
  api_instance.get_resource_batches(opts)
rescue DdrClient::ApiError => e
  puts "Exception when calling ResourcesApi->get_resource_batches: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batch_objects** | [****](.md)| Include batch objects | [optional] [default to false]
 **page** | [****](.md)| Page number of results to return (default: 1) in the JSON response | [optional] 
 **per_page** | [****](.md)| Number of records to return per page (default: 20) in the JSON response | [optional] 

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/csv



# **get_resource_children**
> get_resource_children(opts)



Retrieve resource children

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::ResourcesApi.new
opts = { 
  fields: DdrClient::null.new, #  | Comma-separated list of metadata fields to include in the response. All fields are included if the parameter is omitted, unless the 'exclude_fields' parameter is used.
  exclude_fields: DdrClient::null.new, #  | Comma-separated list of metadata fields to exclude from the response. Cannot be used in combination with the 'fields' parameter.
  csv_fields: DdrClient::null.new, #  | Comma-separated list of additional fields to include in the CSV response
  file_fields: DdrClient::null.new, #  | Comma-separated list of file fields to include in the CSV response
  remove_empty_columns: DdrClient::null.new, #  | Remove empty columns from the CSV response
  page: DdrClient::null.new, #  | Page number of results to return (default: 1) in the JSON response
  per_page: DdrClient::null.new #  | Number of records to return per page (default: 20) in the JSON response
}

begin
  api_instance.get_resource_children(opts)
rescue DdrClient::ApiError => e
  puts "Exception when calling ResourcesApi->get_resource_children: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | [****](.md)| Comma-separated list of metadata fields to include in the response. All fields are included if the parameter is omitted, unless the &#x27;exclude_fields&#x27; parameter is used. | [optional] 
 **exclude_fields** | [****](.md)| Comma-separated list of metadata fields to exclude from the response. Cannot be used in combination with the &#x27;fields&#x27; parameter. | [optional] 
 **csv_fields** | [****](.md)| Comma-separated list of additional fields to include in the CSV response | [optional] 
 **file_fields** | [****](.md)| Comma-separated list of file fields to include in the CSV response | [optional] 
 **remove_empty_columns** | [****](.md)| Remove empty columns from the CSV response | [optional] [default to false]
 **page** | [****](.md)| Page number of results to return (default: 1) in the JSON response | [optional] 
 **per_page** | [****](.md)| Number of records to return per page (default: 20) in the JSON response | [optional] 

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined



# **get_resource_download**
> get_resource_download



Download resource original file

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::ResourcesApi.new

begin
  api_instance.get_resource_download
rescue DdrClient::ApiError => e
  puts "Exception when calling ResourcesApi->get_resource_download: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream



# **get_resource_file**
> get_resource_file



Download the named file from the resource

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::ResourcesApi.new

begin
  api_instance.get_resource_file
rescue DdrClient::ApiError => e
  puts "Exception when calling ResourcesApi->get_resource_file: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream



# **get_resource_files**
> get_resource_files



Retrieve resource files

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::ResourcesApi.new

begin
  api_instance.get_resource_files
rescue DdrClient::ApiError => e
  puts "Exception when calling ResourcesApi->get_resource_files: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_resource_fixity**
> get_resource_fixity



Perform a resource fixity check

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::ResourcesApi.new

begin
  api_instance.get_resource_fixity
rescue DdrClient::ApiError => e
  puts "Exception when calling ResourcesApi->get_resource_fixity: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined



# **get_resource_fixity_check**
> get_resource_fixity_check



Perform a resource fixity check

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::ResourcesApi.new

begin
  api_instance.get_resource_fixity_check
rescue DdrClient::ApiError => e
  puts "Exception when calling ResourcesApi->get_resource_fixity_check: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_resource_members**
> get_resource_members(opts)



Retrieve resource members

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::ResourcesApi.new
opts = { 
  model: DdrClient::null.new, #  | Filter by resource model
  fields: DdrClient::null.new, #  | Comma-separated list of metadata fields to include in the response. All fields are included if the parameter is omitted, unless the 'exclude_fields' parameter is used.
  exclude_fields: DdrClient::null.new, #  | Comma-separated list of metadata fields to exclude from the response. Cannot be used in combination with the 'fields' parameter.
  csv_fields: DdrClient::null.new, #  | Comma-separated list of additional fields to include in the CSV response
  file_fields: DdrClient::null.new, #  | Comma-separated list of file fields to include in the CSV response
  remove_empty_columns: DdrClient::null.new, #  | Remove empty columns from the CSV response
  page: DdrClient::null.new, #  | Page number of results to return (default: 1) in the JSON response
  per_page: DdrClient::null.new #  | Number of records to return per page (default: 20) in the JSON response
}

begin
  api_instance.get_resource_members(opts)
rescue DdrClient::ApiError => e
  puts "Exception when calling ResourcesApi->get_resource_members: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [****](.md)| Filter by resource model | [optional] 
 **fields** | [****](.md)| Comma-separated list of metadata fields to include in the response. All fields are included if the parameter is omitted, unless the &#x27;exclude_fields&#x27; parameter is used. | [optional] 
 **exclude_fields** | [****](.md)| Comma-separated list of metadata fields to exclude from the response. Cannot be used in combination with the &#x27;fields&#x27; parameter. | [optional] 
 **csv_fields** | [****](.md)| Comma-separated list of additional fields to include in the CSV response | [optional] 
 **file_fields** | [****](.md)| Comma-separated list of file fields to include in the CSV response | [optional] 
 **remove_empty_columns** | [****](.md)| Remove empty columns from the CSV response | [optional] [default to false]
 **page** | [****](.md)| Page number of results to return (default: 1) in the JSON response | [optional] 
 **per_page** | [****](.md)| Number of records to return per page (default: 20) in the JSON response | [optional] 

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined



# **get_resource_permanent_id**
> get_resource_permanent_id



Retrieve resource permanent ID metadata

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::ResourcesApi.new

begin
  api_instance.get_resource_permanent_id
rescue DdrClient::ApiError => e
  puts "Exception when calling ResourcesApi->get_resource_permanent_id: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_resource_permissions**
> get_resource_permissions



Retrieve resource permissions

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::ResourcesApi.new

begin
  api_instance.get_resource_permissions
rescue DdrClient::ApiError => e
  puts "Exception when calling ResourcesApi->get_resource_permissions: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_resource_targets**
> get_resource_targets(opts)



Retrieve resource targets

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::ResourcesApi.new
opts = { 
  fields: DdrClient::null.new, #  | Comma-separated list of metadata fields to include in the response. All fields are included if the parameter is omitted, unless the 'exclude_fields' parameter is used.
  exclude_fields: DdrClient::null.new, #  | Comma-separated list of metadata fields to exclude from the response. Cannot be used in combination with the 'fields' parameter.
  csv_fields: DdrClient::null.new, #  | Comma-separated list of additional fields to include in the CSV response
  file_fields: DdrClient::null.new, #  | Comma-separated list of file fields to include in the CSV response
  remove_empty_columns: DdrClient::null.new, #  | Remove empty columns from the CSV response
  page: DdrClient::null.new, #  | Page number of results to return (default: 1) in the JSON response
  per_page: DdrClient::null.new #  | Number of records to return per page (default: 20) in the JSON response
}

begin
  api_instance.get_resource_targets(opts)
rescue DdrClient::ApiError => e
  puts "Exception when calling ResourcesApi->get_resource_targets: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | [****](.md)| Comma-separated list of metadata fields to include in the response. All fields are included if the parameter is omitted, unless the &#x27;exclude_fields&#x27; parameter is used. | [optional] 
 **exclude_fields** | [****](.md)| Comma-separated list of metadata fields to exclude from the response. Cannot be used in combination with the &#x27;fields&#x27; parameter. | [optional] 
 **csv_fields** | [****](.md)| Comma-separated list of additional fields to include in the CSV response | [optional] 
 **file_fields** | [****](.md)| Comma-separated list of file fields to include in the CSV response | [optional] 
 **remove_empty_columns** | [****](.md)| Remove empty columns from the CSV response | [optional] [default to false]
 **page** | [****](.md)| Page number of results to return (default: 1) in the JSON response | [optional] 
 **per_page** | [****](.md)| Number of records to return per page (default: 20) in the JSON response | [optional] 

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined



# **get_resource_technical_metadata**
> get_resource_technical_metadata



Get technical metadata for a content-bearing resource, the members of a Collection, or children of an Item

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::ResourcesApi.new

begin
  api_instance.get_resource_technical_metadata
rescue DdrClient::ApiError => e
  puts "Exception when calling ResourcesApi->get_resource_technical_metadata: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/csv



# **get_resources**
> get_resources(opts)



Retrieve resources

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::ResourcesApi.new
opts = { 
  fields: DdrClient::null.new, #  | Comma-separated list of metadata fields to include in the response. All fields are included if the parameter is omitted, unless the 'exclude_fields' parameter is used.
  exclude_fields: DdrClient::null.new, #  | Comma-separated list of metadata fields to exclude from the response. Cannot be used in combination with the 'fields' parameter.
  csv_fields: DdrClient::null.new, #  | Comma-separated list of additional fields to include in the CSV response
  file_fields: DdrClient::null.new, #  | Comma-separated list of file fields to include in the CSV response
  remove_empty_columns: DdrClient::null.new, #  | Remove empty columns from the CSV response
  model: DdrClient::null.new, #  | Filter by model
  admin_set: DdrClient::null.new, #  | Filter by admin set
  original_filename: DdrClient::null.new, #  | Filter by original filename
  sha1: DdrClient::null.new, #  | Filter by SHA1 checksum
  workflow_state: DdrClient::null.new, #  | Filter by workflow state
  modified_before: DdrClient::null.new, #  | Filter by modified before a date/time
  modified_since: DdrClient::null.new, #  | Filter by modified since a date/time
  page: DdrClient::null.new, #  | Page number of results to return (default: 1) in the JSON response
  per_page: DdrClient::null.new #  | Number of records to return per page (default: 20) in the JSON response
}

begin
  api_instance.get_resources(opts)
rescue DdrClient::ApiError => e
  puts "Exception when calling ResourcesApi->get_resources: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | [****](.md)| Comma-separated list of metadata fields to include in the response. All fields are included if the parameter is omitted, unless the &#x27;exclude_fields&#x27; parameter is used. | [optional] 
 **exclude_fields** | [****](.md)| Comma-separated list of metadata fields to exclude from the response. Cannot be used in combination with the &#x27;fields&#x27; parameter. | [optional] 
 **csv_fields** | [****](.md)| Comma-separated list of additional fields to include in the CSV response | [optional] 
 **file_fields** | [****](.md)| Comma-separated list of file fields to include in the CSV response | [optional] 
 **remove_empty_columns** | [****](.md)| Remove empty columns from the CSV response | [optional] [default to false]
 **model** | [****](.md)| Filter by model | [optional] 
 **admin_set** | [****](.md)| Filter by admin set | [optional] 
 **original_filename** | [****](.md)| Filter by original filename | [optional] 
 **sha1** | [****](.md)| Filter by SHA1 checksum | [optional] 
 **workflow_state** | [****](.md)| Filter by workflow state | [optional] 
 **modified_before** | [****](.md)| Filter by modified before a date/time | [optional] 
 **modified_since** | [****](.md)| Filter by modified since a date/time | [optional] 
 **page** | [****](.md)| Page number of results to return (default: 1) in the JSON response | [optional] 
 **per_page** | [****](.md)| Number of records to return per page (default: 20) in the JSON response | [optional] 

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined



# **get_resources_by_role**
> get_resources_by_role(opts)



Retrieve resources by role

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::ResourcesApi.new
opts = { 
  agent: DdrClient::null.new, #  | Filter by role agent
  role_type: DdrClient::null.new, #  | Filter by role type
  scope: DdrClient::null.new, #  | Filter by role scope
  fields: DdrClient::null.new, #  | Comma-separated list of metadata fields to include in the response. All fields are included if the parameter is omitted, unless the 'exclude_fields' parameter is used.
  exclude_fields: DdrClient::null.new, #  | Comma-separated list of metadata fields to exclude from the response. Cannot be used in combination with the 'fields' parameter.
  csv_fields: DdrClient::null.new, #  | Comma-separated list of additional fields to include in the CSV response
  file_fields: DdrClient::null.new, #  | Comma-separated list of file fields to include in the CSV response
  remove_empty_columns: DdrClient::null.new, #  | Remove empty columns from the CSV response
  page: DdrClient::null.new, #  | Page number of results to return (default: 1) in the JSON response
  per_page: DdrClient::null.new #  | Number of records to return per page (default: 20) in the JSON response
}

begin
  api_instance.get_resources_by_role(opts)
rescue DdrClient::ApiError => e
  puts "Exception when calling ResourcesApi->get_resources_by_role: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agent** | [****](.md)| Filter by role agent | [optional] 
 **role_type** | [****](.md)| Filter by role type | [optional] 
 **scope** | [****](.md)| Filter by role scope | [optional] 
 **fields** | [****](.md)| Comma-separated list of metadata fields to include in the response. All fields are included if the parameter is omitted, unless the &#x27;exclude_fields&#x27; parameter is used. | [optional] 
 **exclude_fields** | [****](.md)| Comma-separated list of metadata fields to exclude from the response. Cannot be used in combination with the &#x27;fields&#x27; parameter. | [optional] 
 **csv_fields** | [****](.md)| Comma-separated list of additional fields to include in the CSV response | [optional] 
 **file_fields** | [****](.md)| Comma-separated list of file fields to include in the CSV response | [optional] 
 **remove_empty_columns** | [****](.md)| Remove empty columns from the CSV response | [optional] [default to false]
 **page** | [****](.md)| Page number of results to return (default: 1) in the JSON response | [optional] 
 **per_page** | [****](.md)| Number of records to return per page (default: 20) in the JSON response | [optional] 

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined



# **patch_resource**
> DDRResourceSchema patch_resource(body)



Update resource

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::ResourcesApi.new
body = DdrClient::null.new #  | 


begin
  result = api_instance.patch_resource(body)
  p result
rescue DdrClient::ApiError => e
  puts "Exception when calling ResourcesApi->patch_resource: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [****](.md)|  | 

### Return type

[**DDRResourceSchema**](DDRResourceSchema.md)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **post_resource_technical_metadata**
> post_resource_technical_metadata



Generate technical metadata for the resource original file

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::ResourcesApi.new

begin
  api_instance.post_resource_technical_metadata
rescue DdrClient::ApiError => e
  puts "Exception when calling ResourcesApi->post_resource_technical_metadata: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined



# **post_resources**
> DDRResourceSchema post_resources(body)



Create resource

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::ResourcesApi.new
body = DdrClient::null.new #  | 


begin
  result = api_instance.post_resources(body)
  p result
rescue DdrClient::ApiError => e
  puts "Exception when calling ResourcesApi->post_resources: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [****](.md)|  | 

### Return type

[**DDRResourceSchema**](DDRResourceSchema.md)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **put_resource_file**
> put_resource_file(filesha1)



Create or update a named file on the resource

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::ResourcesApi.new
file = DdrClient::null.new #  | 
sha1 = DdrClient::null.new #  | 


begin
  api_instance.put_resource_file(filesha1)
rescue DdrClient::ApiError => e
  puts "Exception when calling ResourcesApi->put_resource_file: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | [****](.md)|  | 
 **sha1** | [****](.md)|  | 

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined



