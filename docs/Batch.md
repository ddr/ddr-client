# DdrClient::Batch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [****](.md) |  | [optional] 
**name** | [****](.md) |  | [optional] 
**description** | [****](.md) |  | [optional] 
**outcome** | [****](.md) |  | [optional] 
**status** | [****](.md) |  | [optional] 
**collection_id** | [****](.md) |  | [optional] 
**collection_title** | [****](.md) |  | [optional] 
**created_at** | [****](.md) |  | [optional] 
**updated_at** | [****](.md) |  | [optional] 
**software_version** | [****](.md) | Software version used for the batch operation | [optional] 
**started_at** | [****](.md) |  | [optional] 
**stopped_at** | [****](.md) |  | [optional] 
**processing_started_at** | [****](.md) |  | [optional] 
**user** | [**User**](User.md) |  | [optional] 
**logfile** | [**BatchLogFile**](BatchLogFile.md) |  | [optional] 
**batch_objects** | [****](.md) |  | [optional] 

