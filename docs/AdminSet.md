# DdrClient::AdminSet

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | [****](.md) | The unique identifier for the AdminSet | 
**title** | [****](.md) | The human-readable title of the AdminSet | 

