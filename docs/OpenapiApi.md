# DdrClient::OpenapiApi

All URIs are relative to *https://ddr-admin.lib.duke.edu/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_openapi**](OpenapiApi.md#get_openapi) | **GET** /openapi | 
[**get_swagger_doc**](OpenapiApi.md#get_swagger_doc) | **GET** /swagger_doc | 

# **get_openapi**
> get_openapi



Retrieve OpenAPI document

### Example
```ruby
# load the gem
require 'ddr_client'

api_instance = DdrClient::OpenapiApi.new

begin
  api_instance.get_openapi
rescue DdrClient::ApiError => e
  puts "Exception when calling OpenapiApi->get_openapi: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_swagger_doc**
> get_swagger_doc



Retrieve Swagger/OpenAPI document

### Example
```ruby
# load the gem
require 'ddr_client'

api_instance = DdrClient::OpenapiApi.new

begin
  api_instance.get_swagger_doc
rescue DdrClient::ApiError => e
  puts "Exception when calling OpenapiApi->get_swagger_doc: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined



