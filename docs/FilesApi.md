# DdrClient::FilesApi

All URIs are relative to *https://ddr-admin.lib.duke.edu/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_resource_download**](FilesApi.md#get_resource_download) | **GET** /resources/{id}/download | 
[**get_resource_file**](FilesApi.md#get_resource_file) | **GET** /resources/{id}/files/{file_field} | 
[**get_resource_files**](FilesApi.md#get_resource_files) | **GET** /resources/{id}/files | 
[**put_resource_file**](FilesApi.md#put_resource_file) | **PUT** /resources/{id}/files/{file_field} | 

# **get_resource_download**
> get_resource_download



Download resource original file

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::FilesApi.new

begin
  api_instance.get_resource_download
rescue DdrClient::ApiError => e
  puts "Exception when calling FilesApi->get_resource_download: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream



# **get_resource_file**
> get_resource_file



Download the named file from the resource

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::FilesApi.new

begin
  api_instance.get_resource_file
rescue DdrClient::ApiError => e
  puts "Exception when calling FilesApi->get_resource_file: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream



# **get_resource_files**
> get_resource_files



Retrieve resource files

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::FilesApi.new

begin
  api_instance.get_resource_files
rescue DdrClient::ApiError => e
  puts "Exception when calling FilesApi->get_resource_files: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **put_resource_file**
> put_resource_file(filesha1)



Create or update a named file on the resource

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::FilesApi.new
file = DdrClient::null.new #  | 
sha1 = DdrClient::null.new #  | 


begin
  api_instance.put_resource_file(filesha1)
rescue DdrClient::ApiError => e
  puts "Exception when calling FilesApi->put_resource_file: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | [****](.md)|  | 
 **sha1** | [****](.md)|  | 

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined



