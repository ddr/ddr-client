# DdrClient::UsersApi

All URIs are relative to *https://ddr-admin.lib.duke.edu/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_user**](UsersApi.md#get_user) | **GET** /users/{id} | 
[**get_users**](UsersApi.md#get_users) | **GET** /users | 
[**post_users**](UsersApi.md#post_users) | **POST** /users | 

# **get_user**
> User get_user



Retrieve user

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::UsersApi.new

begin
  result = api_instance.get_user
  p result
rescue DdrClient::ApiError => e
  puts "Exception when calling UsersApi->get_user: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**User**](User.md)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_users**
> get_users(opts)



Retrieve users

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::UsersApi.new
opts = { 
  username: DdrClient::null.new, #  | Query/filter by username
  netid: DdrClient::null.new, #  | Query/filter by NetID
  duid: DdrClient::null.new, #  | Query/filter by Duke Unique ID
  email: DdrClient::null.new, #  | Query/filter by email address
  first_name: DdrClient::null.new, #  | Query/filter by first name
  last_name: DdrClient::null.new, #  | Query/filter by last name
  per_page: DdrClient::null.new, #  | Number of records to return per page (default: 20) in the JSON response
  page: DdrClient::null.new #  | Page number of results to return (default: 1) in the JSON response
}

begin
  api_instance.get_users(opts)
rescue DdrClient::ApiError => e
  puts "Exception when calling UsersApi->get_users: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | [****](.md)| Query/filter by username | [optional] 
 **netid** | [****](.md)| Query/filter by NetID | [optional] 
 **duid** | [****](.md)| Query/filter by Duke Unique ID | [optional] 
 **email** | [****](.md)| Query/filter by email address | [optional] 
 **first_name** | [****](.md)| Query/filter by first name | [optional] 
 **last_name** | [****](.md)| Query/filter by last name | [optional] 
 **per_page** | [****](.md)| Number of records to return per page (default: 20) in the JSON response | [optional] 
 **page** | [****](.md)| Page number of results to return (default: 1) in the JSON response | [optional] 

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/csv



# **post_users**
> User post_users(body)



Create a user in a non-production environment

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::UsersApi.new
body = DdrClient::User.new # User | 


begin
  result = api_instance.post_users(body)
  p result
rescue DdrClient::ApiError => e
  puts "Exception when calling UsersApi->post_users: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**User**](User.md)|  | 

### Return type

[**User**](User.md)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



