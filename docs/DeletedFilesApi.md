# DdrClient::DeletedFilesApi

All URIs are relative to *https://ddr-admin.lib.duke.edu/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_deleted_files**](DeletedFilesApi.md#get_deleted_files) | **GET** /deleted_files | 

# **get_deleted_files**
> get_deleted_files(opts)



Retrieve deleted file records

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::DeletedFilesApi.new
opts = { 
  resource_id: DdrClient::null.new, #  | Filter by DDR resource ID
  file_field: DdrClient::null.new, #  | Filter by resource file field name
  path: DdrClient::null.new, #  | Filter by file path
  since: DdrClient::null.new, #  | Filter files deleted since a point a time
  before: DdrClient::null.new, #  | Filter files deleted up to (before) a point in time
  page: DdrClient::null.new, #  | Page number of results to return (default: 1) in the JSON response
  per_page: DdrClient::null.new #  | Number of records to return per page (default: 20) in the JSON response
}

begin
  api_instance.get_deleted_files(opts)
rescue DdrClient::ApiError => e
  puts "Exception when calling DeletedFilesApi->get_deleted_files: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resource_id** | [****](.md)| Filter by DDR resource ID | [optional] 
 **file_field** | [****](.md)| Filter by resource file field name | [optional] 
 **path** | [****](.md)| Filter by file path | [optional] 
 **since** | [****](.md)| Filter files deleted since a point a time | [optional] 
 **before** | [****](.md)| Filter files deleted up to (before) a point in time | [optional] 
 **page** | [****](.md)| Page number of results to return (default: 1) in the JSON response | [optional] 
 **per_page** | [****](.md)| Number of records to return per page (default: 20) in the JSON response | [optional] 

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/csv



