# DdrClient::Event

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [****](.md) |  | [optional] 
**resource_id** | [****](.md) | ID of the resource to which the event pertains | 
**type** | [****](.md) | Event category | 
**description** | [****](.md) | Generic description of event type | [optional] 
**summary** | [****](.md) | Summary of event action | [optional] 
**event_date_time** | [****](.md) | When the event occurred or was recorded | 
**outcome** | [****](.md) | Event outcome | 
**software** | [****](.md) | Software associated with the event | [optional] 
**comment** | [****](.md) | Optional operator comment | [optional] 
**detail** | [****](.md) | Additional description of or information from the event, such as an exception | [optional] 
**performed_by** | [****](.md) | Person or entity responsible for initiating or causing the event. When no operator is given the value presented is &#x27;SYSTEM&#x27;. | [optional] 
**user_key** | [****](.md) | Representation of the user who performed the action, if any. | [optional] 
**permanent_id** | [****](.md) | Permanent identifier (ARK) assigned to the resource | [optional] 
**user_id** | [****](.md) | ID of the user who initiated the event | [optional] 
**exception** | [****](.md) | Exception type and message | [optional] 
**pid** | [****](.md) | Fedora 3 PID of the resource | [optional] 

