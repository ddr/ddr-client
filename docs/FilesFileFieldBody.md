# DdrClient::FilesFileFieldBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | [****](.md) | The file to upload (max size: 1 GB) | 
**sha1** | [****](.md) | The SHA1 digest of the file | 

