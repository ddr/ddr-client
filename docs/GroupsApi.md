# DdrClient::GroupsApi

All URIs are relative to *https://ddr-admin.lib.duke.edu/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_groups**](GroupsApi.md#get_groups) | **GET** /groups | 

# **get_groups**
> get_groups(opts)



List groups

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::GroupsApi.new
opts = { 
  include_members: DdrClient::null.new #  | Include members for each group
}

begin
  api_instance.get_groups(opts)
rescue DdrClient::ApiError => e
  puts "Exception when calling GroupsApi->get_groups: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **include_members** | [****](.md)| Include members for each group | [optional] [default to false]

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



