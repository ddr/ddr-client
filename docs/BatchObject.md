# DdrClient::BatchObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [****](.md) |  | [optional] 
**batch_id** | [****](.md) |  | [optional] 
**identifier** | [****](.md) | An identifier, such as local ID, associated with the target resource | [optional] 
**model** | [****](.md) | The (unqualifed) model of the resource that is the target of the batch object | [optional] 
**label** | [****](.md) |  | [optional] 
**resource_id** | [****](.md) | The ID of the resource that is the target of the batch object | [optional] 
**created_at** | [****](.md) |  | [optional] 
**updated_at** | [****](.md) |  | [optional] 
**type** | [****](.md) |  | [optional] 
**verified** | [****](.md) |  | [optional] 
**handled** | [****](.md) |  | [optional] 
**processed** | [****](.md) |  | [optional] 
**validated** | [****](.md) |  | [optional] 
**attributes** | [****](.md) | Metadata attributes of the object | [optional] 
**files** | [****](.md) | Files attached to the object | [optional] 
**messages** | [****](.md) | Messages associated with the object | [optional] 
**relationships** | [****](.md) | Relationships to other objects | [optional] 
**roles** | [****](.md) | Roles assigned to the object | [optional] 

