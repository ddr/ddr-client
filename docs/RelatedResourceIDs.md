# DdrClient::RelatedResourceIDs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**collection_id** | [****](.md) | ID of the Collection of which this resource is a member | [optional] 
**attached_to_id** | [****](.md) | ID of the resource to which this resource is attached | [optional] 
**parent_id** | [****](.md) | ID of the parent of this resource | [optional] 
**target_id** | [****](.md) | ID of the Target associated with this resource | [optional] 

