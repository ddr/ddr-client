# DdrClient::SchemasApi

All URIs are relative to *https://ddr-admin.lib.duke.edu/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_schema**](SchemasApi.md#get_schema) | **GET** /schemas/{entity} | 
[**get_schema_deprecated**](SchemasApi.md#get_schema_deprecated) | **GET** /schema/{entity} | 

# **get_schema**
> get_schema



Retrieve schema for an entity

### Example
```ruby
# load the gem
require 'ddr_client'

api_instance = DdrClient::SchemasApi.new

begin
  api_instance.get_schema
rescue DdrClient::ApiError => e
  puts "Exception when calling SchemasApi->get_schema: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/schema+json



# **get_schema_deprecated**
> get_schema_deprecated



Retrieve schema for an entity

### Example
```ruby
# load the gem
require 'ddr_client'

api_instance = DdrClient::SchemasApi.new

begin
  api_instance.get_schema_deprecated
rescue DdrClient::ApiError => e
  puts "Exception when calling SchemasApi->get_schema_deprecated: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined



