# DdrClient::BatchesApi

All URIs are relative to *https://ddr-admin.lib.duke.edu/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_batch**](BatchesApi.md#get_batch) | **GET** /batches/{id} | 
[**get_batch_log**](BatchesApi.md#get_batch_log) | **GET** /batches/{id}/log | 
[**get_batch_messages**](BatchesApi.md#get_batch_messages) | **GET** /batches/{id}/messages | 
[**get_batch_objects**](BatchesApi.md#get_batch_objects) | **GET** /batches/{id}/batch_objects | 
[**get_batches**](BatchesApi.md#get_batches) | **GET** /batches | 
[**get_resource_batches**](BatchesApi.md#get_resource_batches) | **GET** /resources/{id}/batches | 

# **get_batch**
> Batch get_batch(id, opts)



Retrieve batch

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::BatchesApi.new
id = DdrClient::null.new #  | Record ID
opts = { 
  batch_objects: DdrClient::null.new #  | Include batch objects
}

begin
  result = api_instance.get_batch(id, opts)
  p result
rescue DdrClient::ApiError => e
  puts "Exception when calling BatchesApi->get_batch: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [****](.md)| Record ID | 
 **batch_objects** | [****](.md)| Include batch objects | [optional] [default to false]

### Return type

[**Batch**](Batch.md)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_batch_log**
> get_batch_log(id)



Retrieve batch log

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::BatchesApi.new
id = DdrClient::null.new #  | Record ID


begin
  api_instance.get_batch_log(id)
rescue DdrClient::ApiError => e
  puts "Exception when calling BatchesApi->get_batch_log: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [****](.md)| Record ID | 

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain



# **get_batch_messages**
> get_batch_messages(id, opts)



Retrieve batch messages

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::BatchesApi.new
id = DdrClient::null.new #  | Record ID
opts = { 
  page: DdrClient::null.new, #  | Page number of results to return (default: 1) in the JSON response
  per_page: DdrClient::null.new #  | Number of records to return per page (default: 20) in the JSON response
}

begin
  api_instance.get_batch_messages(id, opts)
rescue DdrClient::ApiError => e
  puts "Exception when calling BatchesApi->get_batch_messages: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [****](.md)| Record ID | 
 **page** | [****](.md)| Page number of results to return (default: 1) in the JSON response | [optional] 
 **per_page** | [****](.md)| Number of records to return per page (default: 20) in the JSON response | [optional] 

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/csv



# **get_batch_objects**
> get_batch_objects(id, opts)



Retrieve batch objects for a batch

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::BatchesApi.new
id = DdrClient::null.new #  | Record ID
opts = { 
  batch_object_details: DdrClient::null.new, #  | [JSON only] Include related attributes, files, messages, relationships, and roles for each batch object
  page: DdrClient::null.new, #  | Page number of results to return (default: 1) in the JSON response
  per_page: DdrClient::null.new #  | Number of records to return per page (default: 20) in the JSON response
}

begin
  api_instance.get_batch_objects(id, opts)
rescue DdrClient::ApiError => e
  puts "Exception when calling BatchesApi->get_batch_objects: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [****](.md)| Record ID | 
 **batch_object_details** | [****](.md)| [JSON only] Include related attributes, files, messages, relationships, and roles for each batch object | [optional] [default to true]
 **page** | [****](.md)| Page number of results to return (default: 1) in the JSON response | [optional] 
 **per_page** | [****](.md)| Number of records to return per page (default: 20) in the JSON response | [optional] 

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/csv



# **get_batches**
> get_batches(opts)



List batches

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::BatchesApi.new
opts = { 
  outcome: DdrClient::null.new, #  | Filter by batch processing outcome
  user_id: DdrClient::null.new, #  | Filter by user ID
  status: DdrClient::null.new, #  | Filter by batch processing status
  page: DdrClient::null.new, #  | Page number of results to return (default: 1) in the JSON response
  per_page: DdrClient::null.new #  | Number of records to return per page (default: 20) in the JSON response
}

begin
  api_instance.get_batches(opts)
rescue DdrClient::ApiError => e
  puts "Exception when calling BatchesApi->get_batches: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **outcome** | [****](.md)| Filter by batch processing outcome | [optional] 
 **user_id** | [****](.md)| Filter by user ID | [optional] 
 **status** | [****](.md)| Filter by batch processing status | [optional] 
 **page** | [****](.md)| Page number of results to return (default: 1) in the JSON response | [optional] 
 **per_page** | [****](.md)| Number of records to return per page (default: 20) in the JSON response | [optional] 

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/csv



# **get_resource_batches**
> get_resource_batches(opts)



Retrieve resource batches

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::BatchesApi.new
opts = { 
  batch_objects: DdrClient::null.new, #  | Include batch objects
  page: DdrClient::null.new, #  | Page number of results to return (default: 1) in the JSON response
  per_page: DdrClient::null.new #  | Number of records to return per page (default: 20) in the JSON response
}

begin
  api_instance.get_resource_batches(opts)
rescue DdrClient::ApiError => e
  puts "Exception when calling BatchesApi->get_resource_batches: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batch_objects** | [****](.md)| Include batch objects | [optional] [default to false]
 **page** | [****](.md)| Page number of results to return (default: 1) in the JSON response | [optional] 
 **per_page** | [****](.md)| Number of records to return per page (default: 20) in the JSON response | [optional] 

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/csv



