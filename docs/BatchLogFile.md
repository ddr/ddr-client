# DdrClient::BatchLogFile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file_name** | [****](.md) |  | [optional] 
**file_size** | [****](.md) |  | [optional] 
**content_type** | [****](.md) |  | [optional] 
**updated_at** | [****](.md) |  | [optional] 

