# DdrClient::BatchObjectsApi

All URIs are relative to *https://ddr-admin.lib.duke.edu/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_batch_object**](BatchObjectsApi.md#get_batch_object) | **GET** /batch_objects/{id} | 
[**get_batch_objects**](BatchObjectsApi.md#get_batch_objects) | **GET** /batches/{id}/batch_objects | 
[**get_batch_objects_0**](BatchObjectsApi.md#get_batch_objects_0) | **GET** /batch_objects | 

# **get_batch_object**
> BatchObject get_batch_object(id)



Retrieve batch object and related attributes, files, messages, relationships, and roles

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::BatchObjectsApi.new
id = DdrClient::null.new #  | Record ID


begin
  result = api_instance.get_batch_object(id)
  p result
rescue DdrClient::ApiError => e
  puts "Exception when calling BatchObjectsApi->get_batch_object: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [****](.md)| Record ID | 

### Return type

[**BatchObject**](BatchObject.md)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_batch_objects**
> get_batch_objects(id, opts)



Retrieve batch objects for a batch

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::BatchObjectsApi.new
id = DdrClient::null.new #  | Record ID
opts = { 
  batch_object_details: DdrClient::null.new, #  | [JSON only] Include related attributes, files, messages, relationships, and roles for each batch object
  page: DdrClient::null.new, #  | Page number of results to return (default: 1) in the JSON response
  per_page: DdrClient::null.new #  | Number of records to return per page (default: 20) in the JSON response
}

begin
  api_instance.get_batch_objects(id, opts)
rescue DdrClient::ApiError => e
  puts "Exception when calling BatchObjectsApi->get_batch_objects: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [****](.md)| Record ID | 
 **batch_object_details** | [****](.md)| [JSON only] Include related attributes, files, messages, relationships, and roles for each batch object | [optional] [default to true]
 **page** | [****](.md)| Page number of results to return (default: 1) in the JSON response | [optional] 
 **per_page** | [****](.md)| Number of records to return per page (default: 20) in the JSON response | [optional] 

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/csv



# **get_batch_objects_0**
> get_batch_objects_0(opts)



Retrieve batch objects

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::BatchObjectsApi.new
opts = { 
  resource_id: DdrClient::null.new, #  | Filter by DDR resource ID
  type: DdrClient::null.new, #  | Filter by batch object type
  model: DdrClient::null.new, #  | Filter by resource model
  page: DdrClient::null.new, #  | Page number of results to return (default: 1) in the JSON response
  per_page: DdrClient::null.new #  | Number of records to return per page (default: 20) in the JSON response
}

begin
  api_instance.get_batch_objects_0(opts)
rescue DdrClient::ApiError => e
  puts "Exception when calling BatchObjectsApi->get_batch_objects_0: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resource_id** | [****](.md)| Filter by DDR resource ID | [optional] 
 **type** | [****](.md)| Filter by batch object type | [optional] 
 **model** | [****](.md)| Filter by resource model | [optional] 
 **page** | [****](.md)| Page number of results to return (default: 1) in the JSON response | [optional] 
 **per_page** | [****](.md)| Number of records to return per page (default: 20) in the JSON response | [optional] 

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/csv



