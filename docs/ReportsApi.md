# DdrClient::ReportsApi

All URIs are relative to *https://ddr-admin.lib.duke.edu/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_collection_summary_report**](ReportsApi.md#get_collection_summary_report) | **GET** /reports/collection_summary | 
[**get_duplicate_content_report**](ReportsApi.md#get_duplicate_content_report) | **GET** /reports/duplicate_content | 
[**get_roles_report**](ReportsApi.md#get_roles_report) | **GET** /reports/roles | 

# **get_collection_summary_report**
> get_collection_summary_report(opts)



Collection summary report

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::ReportsApi.new
opts = { 
  fields: DdrClient::null.new #  | Comma-separated list of fields to include in the report
}

begin
  api_instance.get_collection_summary_report(opts)
rescue DdrClient::ApiError => e
  puts "Exception when calling ReportsApi->get_collection_summary_report: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | [****](.md)| Comma-separated list of fields to include in the report | [optional] 

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/csv



# **get_duplicate_content_report**
> get_duplicate_content_report(opts)



Duplicate content report

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::ReportsApi.new
opts = { 
  fields: DdrClient::null.new #  | Comma-separated list of fields to include in the report
}

begin
  api_instance.get_duplicate_content_report(opts)
rescue DdrClient::ApiError => e
  puts "Exception when calling ReportsApi->get_duplicate_content_report: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | [****](.md)| Comma-separated list of fields to include in the report | [optional] 

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/csv



# **get_roles_report**
> get_roles_report(opts)



Roles report

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::ReportsApi.new
opts = { 
  include_inherited: DdrClient::null.new, #  | Include inherited roles (for Item resources)
  collection_id: DdrClient::null.new, #  | Filter by collection ID
  fields: DdrClient::null.new #  | Comma-separated list of fields to include in the report
}

begin
  api_instance.get_roles_report(opts)
rescue DdrClient::ApiError => e
  puts "Exception when calling ReportsApi->get_roles_report: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **include_inherited** | [****](.md)| Include inherited roles (for Item resources) | [optional] [default to false]
 **collection_id** | [****](.md)| Filter by collection ID | [optional] 
 **fields** | [****](.md)| Comma-separated list of fields to include in the report | [optional] 

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/csv



