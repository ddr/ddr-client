# DdrClient::EventsApi

All URIs are relative to *https://ddr-admin.lib.duke.edu/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_event**](EventsApi.md#get_event) | **GET** /events/{id} | 
[**get_events**](EventsApi.md#get_events) | **GET** /events | 
[**post_events**](EventsApi.md#post_events) | **POST** /events | 

# **get_event**
> Event get_event



Retrieve event

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::EventsApi.new

begin
  result = api_instance.get_event
  p result
rescue DdrClient::ApiError => e
  puts "Exception when calling EventsApi->get_event: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Event**](Event.md)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/xml



# **get_events**
> get_events(opts)



Retrieve events

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::EventsApi.new
opts = { 
  resource_id: DdrClient::null.new, #  | Filter by resource ID (required for XML output)
  type: DdrClient::null.new, #  | Filter by event type
  outcome: DdrClient::null.new, #  | Filter by event outcome
  operator: DdrClient::null.new, #  | Filter by event operator (user name or 'SYSTEM')
  since: DdrClient::null.new, #  | Filter events occurring since a point a time
  before: DdrClient::null.new, #  | Filter events occurring up to (before) a point in time
  per_page: DdrClient::null.new, #  | Number of records to return per page (default: 20) in the JSON response
  page: DdrClient::null.new #  | Page number of results to return (default: 1) in the JSON response
}

begin
  api_instance.get_events(opts)
rescue DdrClient::ApiError => e
  puts "Exception when calling EventsApi->get_events: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resource_id** | [****](.md)| Filter by resource ID (required for XML output) | [optional] 
 **type** | [****](.md)| Filter by event type | [optional] 
 **outcome** | [****](.md)| Filter by event outcome | [optional] 
 **operator** | [****](.md)| Filter by event operator (user name or &#x27;SYSTEM&#x27;) | [optional] 
 **since** | [****](.md)| Filter events occurring since a point a time | [optional] 
 **before** | [****](.md)| Filter events occurring up to (before) a point in time | [optional] 
 **per_page** | [****](.md)| Number of records to return per page (default: 20) in the JSON response | [optional] 
 **page** | [****](.md)| Page number of results to return (default: 1) in the JSON response | [optional] 

### Return type

nil (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/csv, text/xml



# **post_events**
> Event post_events(body)



Create event

### Example
```ruby
# load the gem
require 'ddr_client'
# setup authorization
DdrClient.configure do |config|

end

api_instance = DdrClient::EventsApi.new
body = DdrClient::Event.new # Event | 


begin
  result = api_instance.post_events(body)
  p result
rescue DdrClient::ApiError => e
  puts "Exception when calling EventsApi->post_events: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Event**](Event.md)|  | 

### Return type

[**Event**](Event.md)

### Authorization

[oauth2](../README.md#oauth2), [token](../README.md#token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



