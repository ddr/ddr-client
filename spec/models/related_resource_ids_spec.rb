=begin
#Duke Digital Repostiory API

#Duke Digital Repostiory API

OpenAPI spec version: 1.0.0

Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 3.0.54
=end

require 'spec_helper'
require 'json'
require 'date'

# Unit tests for DdrClient::RelatedResourceIDs
# Automatically generated by swagger-codegen (github.com/swagger-api/swagger-codegen)
# Please update as you see appropriate
describe 'RelatedResourceIDs' do
  before do
    # run before each test
    @instance = DdrClient::RelatedResourceIDs.new
  end

  after do
    # run after each test
  end

  describe 'test an instance of RelatedResourceIDs' do
    it 'should create an instance of RelatedResourceIDs' do
      expect(@instance).to be_instance_of(DdrClient::RelatedResourceIDs)
    end
  end
  describe 'test attribute "collection_id"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "attached_to_id"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "parent_id"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "target_id"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

end
