=begin
#Duke Digital Repostiory API

#Duke Digital Repostiory API

OpenAPI spec version: 1.0.0

Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 3.0.54
=end

require 'spec_helper'
require 'json'

# Unit tests for DdrClient::DeletedFilesApi
# Automatically generated by swagger-codegen (github.com/swagger-api/swagger-codegen)
# Please update as you see appropriate
describe 'DeletedFilesApi' do
  before do
    # run before each test
    @instance = DdrClient::DeletedFilesApi.new
  end

  after do
    # run after each test
  end

  describe 'test an instance of DeletedFilesApi' do
    it 'should create an instance of DeletedFilesApi' do
      expect(@instance).to be_instance_of(DdrClient::DeletedFilesApi)
    end
  end

  # unit tests for get_deleted_files
  # Retrieve deleted file records
  # @param [Hash] opts the optional parameters
  # @option opts [] :resource_id Filter by DDR resource ID
  # @option opts [] :file_field Filter by resource file field name
  # @option opts [] :path Filter by file path
  # @option opts [] :since Filter files deleted since a point a time
  # @option opts [] :before Filter files deleted up to (before) a point in time
  # @option opts [] :page Page number of results to return (default: 1) in the JSON response
  # @option opts [] :per_page Number of records to return per page (default: 20) in the JSON response
  # @return [nil]
  describe 'get_deleted_files test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

end
