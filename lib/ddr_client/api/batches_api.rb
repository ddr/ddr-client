=begin
#Duke Digital Repostiory API

#Duke Digital Repostiory API

OpenAPI spec version: 1.0.0

Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 3.0.54
=end

module DdrClient
  class BatchesApi
    attr_accessor :api_client

    def initialize(api_client = ApiClient.default)
      @api_client = api_client
    end
    # Retrieve batch
    # @param id Record ID
    # @param [Hash] opts the optional parameters
    # @option opts [] :batch_objects Include batch objects (default to false)
    # @return [Batch]
    def get_batch(id, opts = {})
      data, _status_code, _headers = get_batch_with_http_info(id, opts)
      data
    end

    # Retrieve batch
    # @param id Record ID
    # @param [Hash] opts the optional parameters
    # @option opts [] :batch_objects Include batch objects
    # @return [Array<(Batch, Integer, Hash)>] Batch data, response status code and response headers
    def get_batch_with_http_info(id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: BatchesApi.get_batch ...'
      end
      # verify the required parameter 'id' is set
      if @api_client.config.client_side_validation && id.nil?
        fail ArgumentError, "Missing the required parameter 'id' when calling BatchesApi.get_batch"
      end
      # resource path
      local_var_path = '/batches/{id}'.sub('{' + 'id' + '}', id.to_s)

      # query parameters
      query_params = opts[:query_params] || {}
      query_params[:'batch_objects'] = opts[:'batch_objects'] if !opts[:'batch_objects'].nil?

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      return_type = opts[:return_type] || 'Batch' 

      auth_names = opts[:auth_names] || ['oauth2', 'token']
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type)

      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: BatchesApi#get_batch\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Retrieve batch log
    # @param id Record ID
    # @param [Hash] opts the optional parameters
    # @return [nil]
    def get_batch_log(id, opts = {})
      get_batch_log_with_http_info(id, opts)
      nil
    end

    # Retrieve batch log
    # @param id Record ID
    # @param [Hash] opts the optional parameters
    # @return [Array<(nil, Integer, Hash)>] nil, response status code and response headers
    def get_batch_log_with_http_info(id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: BatchesApi.get_batch_log ...'
      end
      # verify the required parameter 'id' is set
      if @api_client.config.client_side_validation && id.nil?
        fail ArgumentError, "Missing the required parameter 'id' when calling BatchesApi.get_batch_log"
      end
      # resource path
      local_var_path = '/batches/{id}/log'.sub('{' + 'id' + '}', id.to_s)

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['text/plain'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      return_type = opts[:return_type] 

      auth_names = opts[:auth_names] || ['oauth2', 'token']
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type)

      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: BatchesApi#get_batch_log\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Retrieve batch messages
    # @param id Record ID
    # @param [Hash] opts the optional parameters
    # @option opts [] :page Page number of results to return (default: 1) in the JSON response
    # @option opts [] :per_page Number of records to return per page (default: 20) in the JSON response
    # @return [nil]
    def get_batch_messages(id, opts = {})
      get_batch_messages_with_http_info(id, opts)
      nil
    end

    # Retrieve batch messages
    # @param id Record ID
    # @param [Hash] opts the optional parameters
    # @option opts [] :page Page number of results to return (default: 1) in the JSON response
    # @option opts [] :per_page Number of records to return per page (default: 20) in the JSON response
    # @return [Array<(nil, Integer, Hash)>] nil, response status code and response headers
    def get_batch_messages_with_http_info(id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: BatchesApi.get_batch_messages ...'
      end
      # verify the required parameter 'id' is set
      if @api_client.config.client_side_validation && id.nil?
        fail ArgumentError, "Missing the required parameter 'id' when calling BatchesApi.get_batch_messages"
      end
      # resource path
      local_var_path = '/batches/{id}/messages'.sub('{' + 'id' + '}', id.to_s)

      # query parameters
      query_params = opts[:query_params] || {}
      query_params[:'page'] = opts[:'page'] if !opts[:'page'].nil?
      query_params[:'per_page'] = opts[:'per_page'] if !opts[:'per_page'].nil?

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json', 'text/csv'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      return_type = opts[:return_type] 

      auth_names = opts[:auth_names] || ['oauth2', 'token']
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type)

      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: BatchesApi#get_batch_messages\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Retrieve batch objects for a batch
    # @param id Record ID
    # @param [Hash] opts the optional parameters
    # @option opts [] :batch_object_details [JSON only] Include related attributes, files, messages, relationships, and roles for each batch object (default to true)
    # @option opts [] :page Page number of results to return (default: 1) in the JSON response
    # @option opts [] :per_page Number of records to return per page (default: 20) in the JSON response
    # @return [nil]
    def get_batch_objects(id, opts = {})
      get_batch_objects_with_http_info(id, opts)
      nil
    end

    # Retrieve batch objects for a batch
    # @param id Record ID
    # @param [Hash] opts the optional parameters
    # @option opts [] :batch_object_details [JSON only] Include related attributes, files, messages, relationships, and roles for each batch object
    # @option opts [] :page Page number of results to return (default: 1) in the JSON response
    # @option opts [] :per_page Number of records to return per page (default: 20) in the JSON response
    # @return [Array<(nil, Integer, Hash)>] nil, response status code and response headers
    def get_batch_objects_with_http_info(id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: BatchesApi.get_batch_objects ...'
      end
      # verify the required parameter 'id' is set
      if @api_client.config.client_side_validation && id.nil?
        fail ArgumentError, "Missing the required parameter 'id' when calling BatchesApi.get_batch_objects"
      end
      # resource path
      local_var_path = '/batches/{id}/batch_objects'.sub('{' + 'id' + '}', id.to_s)

      # query parameters
      query_params = opts[:query_params] || {}
      query_params[:'batch_object_details'] = opts[:'batch_object_details'] if !opts[:'batch_object_details'].nil?
      query_params[:'page'] = opts[:'page'] if !opts[:'page'].nil?
      query_params[:'per_page'] = opts[:'per_page'] if !opts[:'per_page'].nil?

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json', 'text/csv'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      return_type = opts[:return_type] 

      auth_names = opts[:auth_names] || ['oauth2', 'token']
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type)

      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: BatchesApi#get_batch_objects\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # List batches
    # @param [Hash] opts the optional parameters
    # @option opts [] :outcome Filter by batch processing outcome
    # @option opts [] :user_id Filter by user ID
    # @option opts [] :status Filter by batch processing status
    # @option opts [] :page Page number of results to return (default: 1) in the JSON response
    # @option opts [] :per_page Number of records to return per page (default: 20) in the JSON response
    # @return [nil]
    def get_batches(opts = {})
      get_batches_with_http_info(opts)
      nil
    end

    # List batches
    # @param [Hash] opts the optional parameters
    # @option opts [] :outcome Filter by batch processing outcome
    # @option opts [] :user_id Filter by user ID
    # @option opts [] :status Filter by batch processing status
    # @option opts [] :page Page number of results to return (default: 1) in the JSON response
    # @option opts [] :per_page Number of records to return per page (default: 20) in the JSON response
    # @return [Array<(nil, Integer, Hash)>] nil, response status code and response headers
    def get_batches_with_http_info(opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: BatchesApi.get_batches ...'
      end
      # resource path
      local_var_path = '/batches'

      # query parameters
      query_params = opts[:query_params] || {}
      query_params[:'outcome'] = opts[:'outcome'] if !opts[:'outcome'].nil?
      query_params[:'user_id'] = opts[:'user_id'] if !opts[:'user_id'].nil?
      query_params[:'status'] = opts[:'status'] if !opts[:'status'].nil?
      query_params[:'page'] = opts[:'page'] if !opts[:'page'].nil?
      query_params[:'per_page'] = opts[:'per_page'] if !opts[:'per_page'].nil?

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json', 'text/csv'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      return_type = opts[:return_type] 

      auth_names = opts[:auth_names] || ['oauth2', 'token']
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type)

      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: BatchesApi#get_batches\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Retrieve resource batches
    # @param [Hash] opts the optional parameters
    # @option opts [] :batch_objects Include batch objects (default to false)
    # @option opts [] :page Page number of results to return (default: 1) in the JSON response
    # @option opts [] :per_page Number of records to return per page (default: 20) in the JSON response
    # @return [nil]
    def get_resource_batches(opts = {})
      get_resource_batches_with_http_info(opts)
      nil
    end

    # Retrieve resource batches
    # @param [Hash] opts the optional parameters
    # @option opts [] :batch_objects Include batch objects
    # @option opts [] :page Page number of results to return (default: 1) in the JSON response
    # @option opts [] :per_page Number of records to return per page (default: 20) in the JSON response
    # @return [Array<(nil, Integer, Hash)>] nil, response status code and response headers
    def get_resource_batches_with_http_info(opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: BatchesApi.get_resource_batches ...'
      end
      # resource path
      local_var_path = '/resources/{id}/batches'

      # query parameters
      query_params = opts[:query_params] || {}
      query_params[:'batch_objects'] = opts[:'batch_objects'] if !opts[:'batch_objects'].nil?
      query_params[:'page'] = opts[:'page'] if !opts[:'page'].nil?
      query_params[:'per_page'] = opts[:'per_page'] if !opts[:'per_page'].nil?

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json', 'text/csv'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      return_type = opts[:return_type] 

      auth_names = opts[:auth_names] || ['oauth2', 'token']
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type)

      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: BatchesApi#get_resource_batches\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
  end
end
