# -*- encoding: utf-8 -*-

=begin
#Duke Digital Repostiory API

#Duke Digital Repostiory API

OpenAPI spec version: 1.0.0

Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 3.0.54
=end

$:.push File.expand_path("../lib", __FILE__)
require "ddr_client/version"

Gem::Specification.new do |s|
  s.name        = "ddr_client"
  s.version     = DdrClient::VERSION
  s.platform    = Gem::Platform::RUBY
  s.authors     = ["Duke University Libraries"]
  s.email       = ["david.chandek.stark@duke.edu"]
  s.homepage    = "https://gitlab.oit.duke.edu/ddr/ddr-client"
  s.summary     = "Duke Digital Repository API client"
  s.description = "Duke Digital Repository API client generated by swagger-codegen."
  s.license     = 'BSD-3-Clause'
  s.required_ruby_version = ">= 1.9"

  s.add_runtime_dependency 'typhoeus', '~> 1.0', '>= 1.0.1'
  s.add_runtime_dependency 'json', '~> 2.1', '>= 2.1.0'

  s.add_development_dependency 'rspec', '~> 3.6', '>= 3.6.0'

  s.files         = `find *`.split("\n").uniq.sort.select { |f| !f.empty? }
  s.test_files    = `find spec/*`.split("\n")
  s.executables   = []
  s.require_paths = ["lib"]
end
