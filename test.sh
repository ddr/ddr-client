#!/bin/bash

set -eux

BUNDLER_VERSION="$(tail -1 Gemfile.lock | awk '{print $$1}')"

docker run --rm -v "$(pwd):/src" -w /src ruby:${ruby_version} /bin/bash -c \
       'gem install bundler && bundle install && bundle exec rake'
